import styled from "styled-components";

const Home = () => {
  return (
    <Styled.Container className="home-page">
      <strong>Home page</strong>
    </Styled.Container>
  );
};

export default Home;

const Styled = {
  Container: styled.div``,
};
