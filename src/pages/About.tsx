import styled from "styled-components";

const About = () => {
  return (
    <Styled.Container className="about-page">
      <strong>About page</strong>
    </Styled.Container>
  );
};

export default About;

const Styled = {
  Container: styled.div``,
};
