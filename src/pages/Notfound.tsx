import styled from "styled-components";

const Notfound = () => {
  return (
    <Styled.Container className="not-found-page">
      <strong>Notfound page</strong>
    </Styled.Container>
  );
};

export default Notfound;

const Styled = {
  Container: styled.div``,
};
